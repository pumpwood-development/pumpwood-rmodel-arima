require(reshape2)


#' Create the xreg to ARIMA/Auto ARIMA models
#'
#' @examples
#' build_x_matrix(my_data_frame, 'ATT_1_')
#' @export
build_x_matrix <- function(data_frame, dependent_column){
    names_to_formula <- names(data_frame)[-which(names(data_frame) %in% c('modeling_unit_id', 'time', 'geoarea_id', dependent_column) )]

    class_cols <- unlist(lapply(data_frame, class))
    factor_names <- names(class_cols[class_cols == "factor"])
    cols_to_remove <- paste(factor_names, "0", sep="")
    x_matrix <- NULL
    if (length(names_to_formula) !=0){
        fomula_text <- paste( "~", paste(names_to_formula, collapse="+"), "-1")
        options(na.action='na.pass')
        x_matrix <- model.matrix(as.formula(fomula_text), data_frame)
        options(na.action='na.omit')
    }

    x_matrix <- x_matrix[, !(colnames(x_matrix) %in% cols_to_remove) ]
    return (x_matrix)
}


#' Introduce NA between diffrent modeling_unit and geoareas
#'
#' Create a row of NA with length max_lag between diffente modeling_unit and geoareas.
#' If there is only one geoarea_id per time unit, it will not be considerated for NA
#' introduction
#' 
#' @param pivoted_data Data in which will be introduced the rows of NA. Originated from action pivot_model_queue_data from ModelQueue
#' @param max_lag Number of NA rows to be introduced
#' @param dependent_column Name of the dependent column to be consired as dependent variable (will not be included in x_matrix)
#' 
#' @return list x_matrix (regression matrix to be used in Arima models), data (original data with the NA rows)
#' , unique_geo (if each geoarea_id have only one data per time unit)
#'
#' @examples
#' build_x_matrix(my_pivoted_dataframe, 14, 'ATT_10_')
#' @export
insert_na_rows <- function (pivoted_data=data.frame(), max_lag=numeric(), dependent_column=character()){
    aggr_mod_geo <- aggregate(geoarea_id ~ modeling_unit_id + time, data=pivoted_data, FUN=length)
    unique_geo <- all(aggr_mod_geo[,'geoarea_id'] == 1)

    splied_data <- NULL
    if (unique_geo){
        splied_data <- split(pivoted_data, pivoted_data[, 'modeling_unit_id'])
    }else{
        splied_data <- split(pivoted_data, pivoted_data[, c('modeling_unit_id', 'geoarea_id')])
    }

    to_combine <- list(splied_data[[1]])
    if (length(splied_data) > 1){
        to_combine_count <- 2
        for ( i_data in 2:length(splied_data) ){
            data_temp <- splied_data[[ i_data ]]
            data_temp <- data_temp[order(data_temp[["time"]]), ]
            names_data <- names(data_temp)
            na_data_frame <- as.data.frame(matrix(NA, ncol=length(names_data), nrow=max_lag))
            names(na_data_frame) <- names_data
            to_combine[[to_combine_count]] <- na_data_frame
            to_combine_count <- to_combine_count + 1

            to_combine[[to_combine_count]] <- data_temp
            to_combine_count <- to_combine_count + 1
        }    
    }

    binded <- do.call("rbind", to_combine)
    x_matrix <- build_x_matrix(data_frame=binded, dependent_column=dependent_column)
    
    return ( list(x_matrix=x_matrix, data=binded, unique_geo=unique_geo ) )
}

#' Calculate the xreg effects
#'
#' Calculate the xreg effects by multipling it by its corresponding values
#' 
#' @param model_coefs Model coef mean
#' @param xreg_matrix Model xreg matrix
#' 
#' @return A matrix with the effects for each xregsc column
#'
#' @examples
#' model_coefs <- coef(my_ArimaModel)
#' xreg_matrix <- my_ArimaModel$xreg
#' arima_xreg_effects(model_coefs, xreg_matrix)
#' @export
arima_xreg_effects <- function(model_coefs, xreg_matrix){
    if ( !is.null(xreg_matrix) ){
        xreg_names   <- colnames(xreg_matrix)
        coef_names   <- names(model_coefs)
        matchs_index <- match(xreg_names, coef_names)
        matchs_index <- matchs_index[!is.na(matchs_index)]
        diag_coefs   <- diag(model_coefs[matchs_index], nrow=length(matchs_index), ncol=length(matchs_index))
        xreg_effects <- xreg_matrix %*% diag_coefs
        colnames(xreg_effects) <- xreg_names
        return (xreg_effects)
    } else{
        return(NULL)
    }
}

#' Returns the variables effects
#'
#' Traduce the model coefs to PumpWood parameters table
#' 
#' @param fitted_transformed Values fitted by the model
#' @param model_coefs Model estimated coeficients
#' @param xreg_matrix xreg matrix used to estimate the model
#' @param modeling_unit_time_geo Index of the modeling units, geography areas and time of model values
#' @param dependent_id Id of the dependent attribute
#' 
#' @return Return a list of effects for each type of model variable: ATT (attributes), FIL (filters), CAL (calendars), AUTO (auto-filters), GEO (geography variables)
#'
#' @examples
#' 
#' @export
queue_attribute_effect <- function(fitted_transformed, model_coefs, xreg_matrix, modeling_unit_time_geo, dependent_id) {
    dependent_var <- sprintf("ATT_%s_", dependent_id)
    xreg_effects  <- arima_xreg_effects(model_coefs, xreg_matrix)
    noise <- fitted_transformed
    matrix_effects <- NULL
    if ( !is.null(xreg_effects) ) {
        noise <- noise - apply(xreg_effects, 1, sum)
        matrix_effects <- data.frame(modeling_unit_time_geo, xreg_effects, as.numeric(noise))
    }else{
        matrix_effects <- data.frame(modeling_unit_time_geo, as.numeric(noise))
    }

    names(matrix_effects)[length(matrix_effects[1,])] <- dependent_var
    
    melted <- melt(matrix_effects, id.vars=c("modeling_unit_id", "time","geoarea_id"))
    melted <- subset(melted, melted[,"value"] !=0 & !is.na(melted[,"modeling_unit_id"]) )

    effects_splited <- split (melted, gsub("_", "",str_extract(melted[,"variable"], "[A-Z]*_")), drop=TRUE)

    if (!is.null(effects_splited$ATT)){

        attribute_id <- as.numeric(gsub("_", "",str_extract(effects_splited$ATT[,"variable"], "_[0-9]*_")))

        sub_variable <- as.numeric(sub("[A-Z]*_[0-9]*_", "", effects_splited$ATT[,"variable"]))
        sub_variable[is.na(sub_variable)] <- -1

        effects_splited$ATT <- data.frame(modeling_unit_id = effects_splited$ATT[, "modeling_unit_id"]
                                        , attribute_out_id = dependent_id
                                        , attribute_id     = attribute_id
                                        , geoarea_id       = effects_splited$ATT[, "geoarea_id"]
                                        , sub_parameter_id = sub_variable
                                        , time             = effects_splited$ATT[, "time"]
                                        , value            = effects_splited$ATT[, "value"])
    }

    if (!is.null(effects_splited$FIL)){
        filter_id <- as.numeric(gsub("_", "",str_extract(effects_splited$FIL[,"variable"], "_[0-9]*_")))
        effects_splited$FIL <- data.frame(modeling_unit_id = effects_splited$FIL[, "modeling_unit_id"]
                                        , attribute_out_id = dependent_id
                                        , filter_id        = filter_id
                                        , geoarea_id       = effects_splited$FIL[, "geoarea_id"]
                                        , time             = effects_splited$FIL[, "time"]
                                        , value            = effects_splited$FIL[, "value"])
    }

    if (!is.null(effects_splited$CAL)){
        calendar_id <- as.numeric(gsub("_", "",str_extract(effects_splited$CAL[,"variable"], "_[0-9]*_")))

        sub_variable <- as.numeric(sub("[A-Z]*_[0-9]*_", "", effects_splited$CAL[,"variable"]))
        sub_variable[is.na(sub_variable)] <- -1

        effects_splited$CAL <- data.frame(modeling_unit_id = effects_splited$CAL[, "modeling_unit_id"]
                                        , attribute_out_id = dependent_id
                                        , calendar_id      = calendar_id
                                        , sub_parameter_id = sub_variable
                                        , geoarea_id       = effects_splited$CAL[, "geoarea_id"]
                                        , time             = effects_splited$CAL[, "time"]
                                        , value            = effects_splited$CAL[, "value"])
    }

    if (!is.null(effects_splited$AUTO)){
        autofilter_id <- as.numeric(gsub("_", "",str_extract(effects_splited$AUTO[,"variable"], "_[0-9]*_")))
        effects_splited$AUTO <- data.frame(modeling_unit_id = effects_splited$AUTO[, "modeling_unit_id"]
                                        , attribute_out_id  = dependent_id
                                        , autofilter_id     = autofilter_id
                                        , geoarea_id        = effects_splited$AUTO[, "geoarea_id"]
                                        , time              = effects_splited$AUTO[, "time"]
                                        , value             = effects_splited$AUTO[, "value"])
    }

    if (!is.null(effects_splited$GEO)){
        geography_id <- as.numeric(gsub("_", "",str_extract(effects_splited$GEO[,"variable"], "_[0-9]*_")))

        sub_variable <- as.numeric(sub("[A-Z]*_[0-9]*_", "", effects_splited$GEO[,"variable"]))
        sub_variable[is.na(sub_variable)] <- -1
        
        effects_splited$GEO <- data.frame(modeling_unit_id = effects_splited$GEO[, "modeling_unit_id"]
                                        , attribute_out_id = dependent_id
                                        , geography_id     = geography_id
                                        , sub_parameter_id = sub_variable
                                        , geoarea_id       = effects_splited$GEO[, "geoarea_id"]
                                        , time             = effects_splited$GEO[, "time"]
                                        , value            = effects_splited$GEO[, "value"])
    }

    return( effects_splited )
}


#' Extract model parameters
#'
#' Extract model parameters to a PumpWood parameter table style
#' 
#' @param model_fited Arima, Auto-Arima fitted model
#' 
#' @return A data.frame contain the paramiters
#'
#' @examples
#' extract_parameters(my_arima_model)
#' @export
extract_parameters <- function (model_fited){
    arima_parameters <- model_fited$arma
    names(arima_parameters) <- c('ar', 'ma', 'sar', 'sma', 'frequency', 'd', 'seaso_d')
    lambda <- model_fited$lambda

    if (is.null(lambda)){
      lambda <- NA
    }

    #Geting and building results of criteria for model selection
    crit_temp <- data.frame(parameter        = c("modelcrit_aic"
                                               , "modelcrit_aicc"
                                               , "modelcrit_bic"
                                               , "modelcrit_rmse"
                                               , "modelcrit_mae"
                                               , "modelcrit_me"
                                               , 'd'
                                               , 'seaso_d'
                                               , 'box-cox'
                                               , 'frequency'
                                               , 'ar'
                                               , 'ma'
                                               , 'sar'
                                               , 'sma')
                          , attribute_id     = NA
                          , filter_id        = NA
                          , calendar_id      = NA
                          , autofilter_id    = NA
                          , geography_id     = NA
                          , sub_parameter_id = NA
                          , result_type      = c('MODEL_SELECTION'
                                               , 'MODEL_SELECTION'
                                               , 'MODEL_SELECTION'
                                               , 'MODEL_SELECTION'
                                               , 'MODEL_SELECTION'
                                               , 'MODEL_SELECTION'
                                               , 'MODEL_AUXILIARY'
                                               , 'MODEL_AUXILIARY'
                                               , 'MODEL_AUXILIARY'
                                               , 'MODEL_AUXILIARY'
                                               , 'MODEL_AUXILIARY'
                                               , 'MODEL_AUXILIARY'
                                               , 'MODEL_AUXILIARY'
                                               , 'MODEL_AUXILIARY')
                          , value            = c(model_fited$aic
                                               , model_fited$aicc
                                               , model_fited$bic
                                               , sqrt(mean(model_fited$residuals^2, na.rm=TRUE))
                                               , mean(abs (model_fited$residuals) , na.rm=TRUE)
                                               , mean(model_fited$residuals       , na.rm=TRUE)
                                               , arima_parameters['d']
                                               , arima_parameters['seaso_d']
                                               , lambda
                                               , arima_parameters['frequency']
                                               , arima_parameters['ar']
                                               , arima_parameters['ma']
                                               , arima_parameters['sar']
                                               , arima_parameters['sma'])
                          , stringsAsFactors = FALSE )

    coef_mean       <- coef(model_fited)
    standard_error  <- sqrt(diag(model_fited$var.coef))
    standard_error  <- standard_error[names(coef_mean)]
    names(standard_error) <- names(coef_mean)
    names_coef_mean       <- names(coef_mean)

    #Breaks the names of the attributes to get if is a model parameter
    #or attribute, filter, geo var, etc
    splited         <- as.data.frame(str_split_fixed(names_coef_mean, "_", n=3), stringsAsFactors=FALSE )
    splited[,-1]    <- lapply(splited[,-1], as.numeric)
    names(splited)  <- c('parameter_name', 'id', 'sub_parameter_id')
    data_frame_pars <- rbind(data.frame(splited, result_type='MEAN'   , values=coef_mean     )
                           , data.frame(splited, result_type='STD_DEV', values=standard_error))

    var_results_index <- data_frame_pars[,'parameter_name'] == "ATT"
    n_var <- sum(var_results_index)
    var_data_frame <- data.frame()
    if (n_var > 0){
        var_data_frame <- data.frame(parameter        = rep(NA, n_var)
                                   , attribute_id     = data_frame_pars[var_results_index, 'id']
                                   , filter_id        = rep(NA, n_var)
                                   , calendar_id      = rep(NA, n_var)
                                   , autofilter_id    = rep(NA, n_var)
                                   , geography_id     = rep(NA, n_var)
                                   , sub_parameter_id = data_frame_pars[var_results_index, 'sub_parameter_id']
                                   , result_type      = data_frame_pars[var_results_index, 'result_type']
                                   , value            = data_frame_pars[var_results_index, 'values']
                                   , stringsAsFactors = FALSE)
    }

    fil_results_index <- data_frame_pars[,'parameter_name'] == "FIL"
    n_fil <- sum(fil_results_index)
    fil_data_frame <- data.frame()
    if (n_fil > 0){
      fil_data_frame <- data.frame(parameter        = rep(NA, n_fil)
                                 , attribute_id     = rep(NA, n_fil)
                                 , filter_id        = data_frame_pars[fil_results_index, 'id']
                                 , calendar_id      = rep(NA, n_fil)
                                 , autofilter_id    = rep(NA, n_fil)
                                 , geography_id     = rep(NA, n_fil)
                                 , sub_parameter_id = data_frame_pars[fil_results_index, 'sub_parameter_id']
                                 , result_type      = data_frame_pars[fil_results_index, 'result_type'     ]
                                 , value            = data_frame_pars[fil_results_index, 'values'          ]
                                 , stringsAsFactors = FALSE)
    }

    cal_results_index <- data_frame_pars[,'parameter_name'] == "CAL"
    n_cal <- sum(cal_results_index)
    cal_data_frame <- data.frame()
    if (n_cal > 0){
        cal_data_frame <- data.frame(parameter        = rep(NA, n_cal)
                                   , attribute_id     = rep(NA, n_cal)
                                   , filter_id        = rep(NA, n_cal)
                                   , calendar_id      = data_frame_pars[cal_results_index, 'id']
                                   , autofilter_id    = rep(NA, n_cal)
                                   , geography_id     = rep(NA, n_cal)
                                   , sub_parameter_id = data_frame_pars[cal_results_index, 'sub_parameter_id']
                                   , result_type      = data_frame_pars[cal_results_index, 'result_type'     ]
                                   , value            = data_frame_pars[cal_results_index, 'values'          ]
                                   , stringsAsFactors = FALSE)
    }

    auto_results_index <- data_frame_pars[,'parameter_name'] == "AUTO"
    n_auto <- sum(auto_results_index)
    auto_data_frame <- data.frame()
    if (n_auto > 0){
      auto_data_frame <- data.frame(parameter       = rep(NA, n_auto)
                                 , attribute_id     = rep(NA, n_auto)
                                 , filter_id        = rep(NA, n_auto)
                                 , calendar_id      = rep(NA, n_auto)
                                 , autofilter_id    = data_frame_pars[auto_results_index, 'id']
                                 , geography_id     = rep(NA, n_auto)
                                 , sub_parameter_id = data_frame_pars[auto_results_index, 'sub_parameter_id']
                                 , result_type      = data_frame_pars[auto_results_index, 'result_type'     ]
                                 , value            = data_frame_pars[auto_results_index, 'values'          ]
                                 , stringsAsFactors = FALSE)
    }

    geo_results_index <- data_frame_pars[,'parameter_name'] == "GEO"
    n_geo <- sum(geo_results_index)
    geo_data_frame <- data.frame()
    if (n_geo > 0){
      geo_data_frame <- data.frame(parameter        = rep(NA, n_geo)
                                 , attribute_id     = rep(NA, n_geo)
                                 , filter_id        = rep(NA, n_geo)
                                 , calendar_id      = rep(NA, n_geo)
                                 , autofilter_id    = rep(NA, n_geo)
                                 , geography_id     = data_frame_pars[geo_results_index, 'id'              ]
                                 , sub_parameter_id = data_frame_pars[geo_results_index, 'sub_parameter_id']
                                 , result_type      = data_frame_pars[geo_results_index, 'result_type'     ]
                                 , value            = data_frame_pars[geo_results_index, 'values'          ]
                                 , stringsAsFactors = FALSE)
    }

    extra_pars <- !(var_results_index | cal_results_index | fil_results_index | auto_results_index | geo_results_index)
    n_extra <- sum(extra_pars)
    extra_data_frame <- data.frame()
    if (n_extra > 0){
      extra_data_frame <- data.frame(parameter        = paste('modelpar_', data_frame_pars[extra_pars, 'parameter_name'], sep="")
                                   , attribute_id     = rep(NA, n_extra)
                                   , filter_id        = rep(NA, n_extra)
                                   , calendar_id      = rep(NA, n_extra)
                                   , autofilter_id    = rep(NA, n_extra)
                                   , geography_id     = rep(NA, n_extra)
                                   , sub_parameter_id = data_frame_pars[extra_pars, 'sub_parameter_id']
                                   , result_type      = data_frame_pars[extra_pars, 'result_type'     ]
                                   , value            = data_frame_pars[extra_pars, 'values'          ]
                                   , stringsAsFactors = FALSE)
    }
    
    data_temp <- rbind (var_data_frame,cal_data_frame,fil_data_frame,auto_data_frame,geo_data_frame,extra_data_frame)
    to_database <- rbind(data_temp, crit_temp)
    
    return(to_database)
}